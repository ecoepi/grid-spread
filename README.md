# Grid Spread

Calculate the local spread velocity based on a grid and identify jumps.

## Required inputs 

input.csv with colums "x" and "y" in a unit of length and "date" in a unit of time (not an actual date).
"cell size" in the same unit as "x" and "y".

## Usage

1. Clone the repository:
   ```bash
   git clone https://git.ufz.de/ecoepi/grid-spread.git
   ```
2. `cd` into the repository:
   ```bash
   cd grid-spread
   ```
3. Build:
   1. Build an `.exe`
      ```bash
      cargo build --bins --release --features="clap csv"
      ```
   1. Build Python bindings
      ```bash
      cargo build --release 
      ```
4. Calculate spread velocity
   ```bash
   grid-spread.exe input.csv --cell-size "cell size" --fields x y date --grid-velocity output.csv
   ```
5. Calculate jumps
   ```bash
   grid-spread.exe input.csv --cell-size "cell size" --fields x y date --jumps output.csv
   ```
