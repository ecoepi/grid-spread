#[cfg(any(feature = "pyo3", test))]
use pyo3::{
    prelude::*,
    types::{PyIterator, PyList},
};

#[cfg(any(feature = "pyo3", test))]
#[pymodule]
#[pyo3(name = "grid_spread")]
fn module_wrapper(_py: Python, m: &PyModule) -> PyResult<()> {
    #[pyclass(name = "GridSpread")]
    #[pyo3(text_signature = "(events, cell_size, /, cell_centre=False)")]
    struct ClassWrapper(GridSpread);

    #[pymethods]
    impl ClassWrapper {
        #[new]
        #[args(cell_centre = "false")]
        pub fn new(events: &PyAny, cell_size: f64, cell_centre: bool) -> Self {
            struct EventsIter<'a>(&'a PyIterator);

            impl<'a> Iterator for EventsIter<'a> {
                type Item = (f64, f64, f64);

                fn next(&mut self) -> Option<Self::Item> {
                    self.0
                        .next()
                        .map(|event| event.unwrap().extract::<(f64, f64, f64)>().unwrap())
                }
            }

            #[derive(Clone)]
            struct Events<'a>(&'a PyAny);

            impl<'a> IntoIterator for Events<'a> {
                type Item = (f64, f64, f64);
                type IntoIter = EventsIter<'a>;

                fn into_iter(self) -> Self::IntoIter {
                    EventsIter(self.0.iter().unwrap())
                }
            }

            Self(GridSpread::new(Events(events), cell_size, cell_centre))
        }

        pub fn origin(&self) -> (f64, f64) {
            self.0.origin()
        }

        pub fn size(&self) -> (isize, isize) {
            self.0.size()
        }

        pub fn grid<'py>(&self, py: Python<'py>) -> &'py PyList {
            PyList::new(py, self.0.grid().map(|row| PyList::new(py, row)))
        }

        pub fn velocity_grid<'py>(&self, py: Python<'py>) -> &'py PyList {
            PyList::new(py, self.0.velocity_grid().map(|row| PyList::new(py, row)))
        }

        pub fn jumps<'py>(&self, py: Python<'py>) -> PyResult<&'py PyList> {
            let jumps = PyList::empty(py);

            for jump in self.0.jumps() {
                jumps.append(jump)?;
            }

            Ok(jumps)
        }
    }

    m.add_class::<ClassWrapper>()
}

pub struct GridSpread {
    transform: Transform,
    grid: Box<[(f64, f64, f64)]>,
    velocity_grid: Box<[f64]>,
}

impl GridSpread {
    pub fn new<E>(events: E, cell_size: f64, cell_centre: bool) -> Self
    where
        E: IntoIterator<Item = (f64, f64, f64)> + Clone,
    {
        let bounds = Bounds::new(events.clone().into_iter());
        let transform = Transform::new(&bounds, cell_size);

        let mut grid = (0..transform.extent())
            .map(|_| (f64::NAN, f64::NAN, f64::NAN))
            .collect::<Box<[_]>>();

        for event in events.into_iter() {
            let (x, y) = transform.apply(&event);
            let idx = transform.flatten(x, y) as usize;
            let cell = &mut grid[idx];

            if cell.2.is_nan() || cell.2 > event.2 {
                *cell = event;
            }
        }

        let mut velocity_grid = (0..transform.extent())
            .map(|_| f64::NAN)
            .collect::<Box<[f64]>>();

        for y0 in 0..transform.y.2 {
            for x0 in 0..transform.x.2 {
                let idx0 = transform.flatten(x0, y0) as usize;
                let cell0 = &grid[idx0];

                if cell0.2.is_nan() {
                    continue;
                }

                for y1 in 0.max(y0 - 1)..transform.y.2.min(y0 + 2) {
                    for x1 in 0.max(x0 - 1)..transform.x.2.min(x0 + 2) {
                        if x0 == x1 && y0 == y1 {
                            continue;
                        }

                        let idx1 = transform.flatten(x1, y1) as usize;
                        let cell1 = &grid[idx1];

                        if cell1.2.is_nan() || cell1.2 <= cell0.2 {
                            continue;
                        }

                        let dx;
                        let dy;

                        if cell_centre {
                            dx = (x1 - x0) as f64 * cell_size;
                            dy = (y1 - y0) as f64 * cell_size
                        } else {
                            dx = cell1.0 - cell0.0;
                            dy = cell1.1 - cell0.1;
                        }

                        let dt = cell1.2 - cell0.2;

                        let v = dx.hypot(dy) / dt;

                        let v1 = &mut velocity_grid[idx1];
                        if v1.is_nan() || *v1 > v {
                            *v1 = v;
                        }
                    }
                }
            }
        }

        Self {
            transform,
            grid,
            velocity_grid,
        }
    }

    pub fn origin(&self) -> (f64, f64) {
        (self.transform.x.0, self.transform.y.0)
    }

    pub fn size(&self) -> (isize, isize) {
        (self.transform.x.2, self.transform.y.2)
    }

    pub fn grid(
        &self,
    ) -> impl ExactSizeIterator<Item = impl ExactSizeIterator<Item = &(f64, f64, f64)> + '_> + '_
    {
        (0..self.transform.y.2).rev().map(move |y| {
            (0..self.transform.x.2).map(move |x| &self.grid[self.transform.flatten(x, y) as usize])
        })
    }

    pub fn velocity_grid(
        &self,
    ) -> impl ExactSizeIterator<Item = impl ExactSizeIterator<Item = &f64> + '_> + '_ {
        (0..self.transform.y.2).rev().map(move |y| {
            (0..self.transform.x.2)
                .map(move |x| &self.velocity_grid[self.transform.flatten(x, y) as usize])
        })
    }

    pub fn jumps(&self) -> impl Iterator<Item = &(f64, f64, f64)> + '_ {
        (0..self.transform.y.2).flat_map(move |y| {
            (0..self.transform.x.2).filter_map(move |x| {
                let v = &self.velocity_grid[self.transform.flatten(x, y) as usize];

                if v.is_nan() {
                    let cell = &self.grid[self.transform.flatten(x, y) as usize];

                    if !cell.2.is_nan() {
                        return Some(cell);
                    }
                }

                None
            })
        })
    }
}

struct Bounds {
    x: (f64, f64),
    y: (f64, f64),
}

impl Bounds {
    fn new<E>(events: E) -> Self
    where
        E: Iterator<Item = (f64, f64, f64)>,
    {
        let mut x = (f64::INFINITY, f64::NEG_INFINITY);
        let mut y = (f64::INFINITY, f64::NEG_INFINITY);

        for event in events {
            x.0 = x.0.min(event.0);
            x.1 = x.1.max(event.0);

            y.0 = y.0.min(event.1);
            y.1 = y.1.max(event.1);
        }

        Self { x, y }
    }
}

struct Transform {
    x: (f64, f64, isize),
    y: (f64, f64, isize),
}

impl Transform {
    fn new(bounds: &Bounds, cell_size: f64) -> Self {
        let x_min = bounds.x.0 - cell_size;
        let x_max = bounds.x.1 + cell_size;

        let x_scale = cell_size.recip();
        let x_extent = ((x_max - x_min) * x_scale).ceil() as isize;

        let x = (x_min, x_scale, x_extent);

        let y_min = bounds.y.0 - cell_size;
        let y_max = bounds.y.1 + cell_size;

        let y_scale = cell_size.recip();
        let y_extent = ((y_max - y_min) * y_scale).ceil() as isize;

        let y = (y_min, y_scale, y_extent);

        Self { x, y }
    }

    fn apply(&self, event: &(f64, f64, f64)) -> (isize, isize) {
        let x = ((event.0 - self.x.0) * self.x.1) as isize;
        let y = ((event.1 - self.y.0) * self.y.1) as isize;

        (x, y)
    }

    fn flatten(&self, x: isize, y: isize) -> isize {
        y * self.x.2 + x
    }

    fn extent(&self) -> isize {
        self.x.2 * self.y.2
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    use pyo3::py_run;

    #[test]
    fn python_bindings() {
        Python::with_gil(|py| {
            let m = PyModule::new(py, "grid_spread").unwrap();
            module_wrapper(py, m).unwrap();
            py_run!(
                py,
                m,
                r#"
                import math

                events = [(0, 0, 0), (1, 1, 1)]
                grid_spread = m.GridSpread(events, 1)

                assert grid_spread.origin() == (-1, -1)
                assert grid_spread.size() == (3, 3)
                
                grid = grid_spread.grid()
                assert grid[1][1] == (0, 0, 0)
                assert grid[0][2] == (1, 1, 1)
                
                velocity_grid = grid_spread.velocity_grid()
                assert not math.isnan(velocity_grid[0][2])

                assert grid_spread.jumps() == [(0, 0, 0)]
            "#
            );
        });
    }
}
