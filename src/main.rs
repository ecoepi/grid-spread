use std::error::Error;
use std::fs::File;
use std::io::{BufWriter, Write};
use std::path::Path;

use clap::{crate_authors, crate_name, crate_version, Arg, Command};
use csv::ReaderBuilder;

use grid_spread::GridSpread;

fn main() -> Fallible {
    let matches = Command::new(crate_name!())
        .version(crate_version!())
        .author(crate_authors!(", "))
        .arg(
            Arg::new("EVENTS")
                .required(true)
                .help("CSV file with events, i.e. x, y and t coordinates"),
        )
        .arg(
            Arg::new("FIELDS")
                .long("fields")
                .required(true)
                .min_values(3)
                .max_values(3),
        )
        .arg(
            Arg::new("CELL_SIZE")
                .long("cell-size")
                .required(true)
                .takes_value(true),
        )
        .arg(Arg::new("CELL_CENTRE").long("cell-centre"))
        .arg(Arg::new("GRID").long("grid").takes_value(true))
        .arg(
            Arg::new("VELOCITY_GRID")
                .long("velocity-grid")
                .takes_value(true),
        )
        .arg(Arg::new("JUMPS").long("jumps").takes_value(true))
        .get_matches();

    let fields = matches.values_of("FIELDS").unwrap().collect::<Vec<_>>();

    let cell_size = matches.value_of("CELL_SIZE").unwrap().parse::<f64>()?;
    let cell_centre = matches.is_present("CELL_CENTRE");

    let events = Events::read(matches.value_of("EVENTS").unwrap(), &fields)?;

    let grid_spread = GridSpread::new(events.0.iter().copied(), cell_size, cell_centre);

    if let Some(path) = matches.value_of("GRID") {
        let mut writer = BufWriter::new(File::create(path)?);

        let (ncols, nrows) = grid_spread.size();
        let (xllcorner, yllcorner) = grid_spread.origin();

        writeln!(&mut writer, "NCOLS {}", ncols)?;
        writeln!(&mut writer, "NROWS {}", nrows)?;
        writeln!(&mut writer, "XLLCORNER {}", xllcorner)?;
        writeln!(&mut writer, "YLLCORNER {}", yllcorner)?;
        writeln!(&mut writer, "CELLSIZE {}", cell_size)?;
        writeln!(&mut writer, "NODATA_VALUE -9999")?;

        for row in grid_spread.grid() {
            for (_, _, t) in row {
                if !t.is_nan() {
                    write!(&mut writer, "{} ", t)?;
                } else {
                    write!(&mut writer, "-9999 ")?;
                }
            }

            writeln!(&mut writer)?;
        }

        writer.flush()?;
    }

    if let Some(path) = matches.value_of("VELOCITY_GRID") {
        let mut writer = BufWriter::new(File::create(path)?);

        let (ncols, nrows) = grid_spread.size();
        let (xllcorner, yllcorner) = grid_spread.origin();

        writeln!(&mut writer, "NCOLS {}", ncols)?;
        writeln!(&mut writer, "NROWS {}", nrows)?;
        writeln!(&mut writer, "XLLCORNER {}", xllcorner)?;
        writeln!(&mut writer, "YLLCORNER {}", yllcorner)?;
        writeln!(&mut writer, "CELLSIZE {}", cell_size)?;
        writeln!(&mut writer, "NODATA_VALUE -9999")?;

        for row in grid_spread.velocity_grid() {
            for v in row {
                if !v.is_nan() {
                    write!(&mut writer, "{} ", v)?;
                } else {
                    write!(&mut writer, "-9999 ")?;
                }
            }

            writeln!(&mut writer)?;
        }

        writer.flush()?;
    }

    if let Some(path) = matches.value_of("JUMPS") {
        let mut writer = BufWriter::new(File::create(path)?);

        writeln!(&mut writer, "{};{};{}", fields[0], fields[1], fields[2])?;

        for (x, y, t) in grid_spread.jumps() {
            writeln!(&mut writer, "{};{};{}", x, y, t)?;
        }

        writer.flush()?;
    }

    Ok(())
}

struct Events(Vec<(f64, f64, f64)>);

impl Events {
    fn read<P: AsRef<Path>>(path: P, fields: &[&str]) -> Fallible<Self> {
        let mut reader = ReaderBuilder::new()
            .delimiter(b';')
            .from_reader(File::open(path)?);

        let headers = reader.headers()?;

        let fields = fields
            .iter()
            .map(|field| {
                headers
                    .iter()
                    .position(|header| *field == header)
                    .ok_or_else(|| format!("Field {} is mssing in CSV file.", field))
            })
            .collect::<Result<Vec<_>, _>>()?;

        let mut events = Vec::new();

        for record in reader.records() {
            let record = record?;

            let x = record
                .get(fields[0])
                .ok_or("Missing x coordinate in CSV record")?
                .parse()?;

            let y = record
                .get(fields[1])
                .ok_or("Missing y coordinate in CSV record")?
                .parse()?;

            let t = record
                .get(fields[2])
                .ok_or("Missing t coordinate in CSV record")?
                .parse()?;

            events.push((x, y, t));
        }

        Ok(Self(events))
    }
}

type Fallible<T = ()> = Result<T, Box<dyn Error>>;
